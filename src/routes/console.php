<?php
use Quantumlab\Router\Router;


/*------PERSON------*/
Router::addRoute('addPerson', 'PersonController@addPerson');
Router::addRoute('removePerson', 'PersonController@removePerson');
Router::addRoute('list', 'PersonController@getPersons');
Router::addRoute('find', 'PersonController@searchByName');

/*------LANGUAGE------*/
Router::addRoute('languages', 'LanguageController@search');
Router::addRoute('addLanguage', 'LanguageController@addLanguage');
Router::addRoute('removeLanguage', 'LanguageController@removeLanguage');
