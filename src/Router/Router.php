<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 06.01.2018
 * Time: 13:32
 */

namespace Quantumlab\Router;


class Router
{
    private static $routes = [];
    public static function addRoute($name, $controller){
        $explodeController = explode('@',$controller);
        self::$routes[] = [
            'name'=>$name,
            'controller'=>$explodeController[0],
            'function'=>$explodeController[1]
        ];
    }
    public static function searchRoute($routeName){

        foreach (self::$routes as $route){
            if(!empty($route['name']) && $route['name'] === $routeName){
                return $route;
            }
        }
        throw new RouterException('Brak zadeklarowanej ścieżki '. $routeName, 404);
    }
}