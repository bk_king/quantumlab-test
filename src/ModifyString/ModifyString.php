<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 07.01.2018
 * Time: 09:46
 */

namespace Quantumlab\ModifyString;


class ModifyString
{
    public static function toUpper($string){
        return mb_strtoupper($string, 'utf-8');
    }
    public static function toLower($string){
        return mb_strtolower($string, 'utf-8');
    }
}