<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 29.12.2017
 * Time: 15:52
 */
namespace Quantumlab\Config;
class Config
{
    const ABSOLUTE_PATH = __DIR__ . '/../';
    const CONTROLLER_NAMESPACE = 'Quantumlab\Controllers\\';
    const DATABASE_PATH = Config::ABSOLUTE_PATH . 'resource/database.json';
}