<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 06.01.2018
 * Time: 13:39
 */

namespace Quantumlab\Controllers;


use Quantumlab\Database\Language;
use Quantumlab\Database\User;

class PersonController
{
    public function getPersons()
    {
        $persons = new User();
        $persons = $persons->getPersons()->get();
       $this->displayUsers($persons);

    }

    private function displayUsers($persons){
        $result = '';
        foreach ($persons as $person) {
            $result .= $person->id.' '. $person->name . ' ' . $person->surname.' - (';
            foreach ($person->languages as $language) {
                $result.=$language->name.' ';
            }
            $result = substr($result, 0, strlen($result)-1).")\n";
        }
        echo $result;

    }
    public function addPerson($data)
    {
        $person = new User();
        $person->store($data);


    }

    public function removePerson($data)
    {
        $person = new User();
        $person->languages();
        $person->remove(['id' => $data[0]]);
    }

    public function searchByName($data)
    {
        $person =  new User();
        $persons = $person->searchByName($data);
        $this->displayUsers($persons);
    }


}