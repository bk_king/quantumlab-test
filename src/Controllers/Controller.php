<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 06.01.2018
 * Time: 13:56
 */

namespace Quantumlab\Controllers;
use Quantumlab\Config\Config;
use Quantumlab\Exception\Controllers\ControllerException;
use Quantumlab\Router\Router;
use Quantumlab\Router\Exception\RouterException;

require_once Config::ABSOLUTE_PATH.'routes/console.php';


class Controller
{
    private $arguments = [];

        public function init($arguments){
            $this->arguments = $this->parseArguments($arguments);
            try{
                $route = Router::searchRoute($this->arguments['routeName']);
                $controllerClass = Config::CONTROLLER_NAMESPACE.$route['controller'];
                $controller = new $controllerClass();
                if(!method_exists($controller, $route['function'])){
                  throw new ControllerException('Brak metody '.$route['function'].' w controllerze '.$route['controller']);
                }
                $this->callControllerFunction($controller, $route);

            }catch(RouterException $e){
                echo $e->getMessage();
            }

        }

        private function callControllerFunction($controller, $route){
            $r = new \ReflectionMethod($controller, $route['function']);
            $params = $r->getParameters();
            if(count($params)>0){
                $controller->{$route['function']}($this->arguments['arguments']);
            }else{
                $controller->{$route['function']}();
            }
        }

        private function parseArguments($arguments){
            $args = [];
            foreach ($arguments as $key => $argument){
                if($key>0){
                    if($key === 1){
                        $args=['routeName'=>$argument];
                    }else{
                        $args['arguments'][]=$argument;
                    }
                }
            }
            return $args;
        }

}