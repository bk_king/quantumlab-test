<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 06.01.2018
 * Time: 14:54
 */

namespace Quantumlab\Exception\Controllers;


use Throwable;

class ControllerException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}