<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 06.01.2018
 * Time: 13:40
 */

namespace Quantumlab\Controllers;


use Quantumlab\Database\Language;

class LanguageController
{
    public function addLanguage($data)
    {
        $language = new Language();
        $language->store($data);
    }

    public function removeLanguage($data)
    {
        $language = new Language();
        $language->persons();
        $lang = $language->where(['name'=>$data[0]])->first();
        if($lang){
            $language->remove(['id'=>$lang->id]);
        }

    }

    public function search($data)
    {
        $language = new Language($data);
        $languages = $language->getLanguages($data);
        $result = '';
        foreach ($languages as $lang) {
            foreach ($lang->persons as $person) {
                $result .= $person->name . ' ' . $person->surname . "\n";
            }

        }
        echo $result;
    }
}