<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 29.12.2017
 * Time: 15:37
 */
namespace Quantumlab\Database;
class Language extends Model
{
    protected $fillable = [
        'name',
    ];
    protected $table = 'languages';

    public function persons()
    {
        return $this->belongsToMany(User::class, UsersLanguages::class, 'language_id', 'user_id');
    }
    public function store($data){
        $this->name = $data[0];
        $this->save();
        return $this;
    }
    public function getLanguages($data){
        $params = [];
        foreach ($data as $param){
            $params['name'] = $param;
            $this->where($params);
        }
        return $this->with('persons')->get();
    }


}