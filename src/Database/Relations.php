<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 07.01.2018
 * Time: 11:07
 */

namespace Quantumlab\Database;


trait Relations
{
    protected $local_key;
    protected $foreign_key;
    protected $relation;
    protected $pivot;
    protected $relationName;
    public function belongsToMany($relation, $pivot_name, $local_key, $foreign_key){
        $this->local_key = $local_key;
        $this->foreign_key = $foreign_key;
        $this->relation = $relation;
        $this->pivot = $pivot_name;
        return $this;
    }
}