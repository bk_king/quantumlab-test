<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 29.12.2017
 * Time: 15:37
 */

namespace Quantumlab\Database;
class User extends Model
{
    protected $fillable = [
        'name',
        'surname'
    ];
    protected $table = 'users';

    public function languages()
    {
        return $this->belongsToMany(Language::class, UsersLanguages::class, 'user_id', 'language_id');
    }

    public function store($data)
    {
        if (!($user = $this->where([
            'name' => $data[0],
            'surname'=>$data[1]
        ])->first())) {
            $this->name = $data[0];
            $this->surname = $data[1];
            $user = $this->save();
        }

        $languages = array_slice($data, 2);

        foreach ($languages as $lang) {
            $language = new Language();
            if (!($result = $language->where(['name' => $lang])->first())) {
                $result = $language->store([0 => $lang]);
            }
            $user->languages()->attach($result->id);
        }

    }

    public function getPersons(){
        return $this->with('languages');
    }

    public function searchByName($data){
        $name = explode(' ', $data[0]);
            $this->where([
                'name'=>$name[0],
                'surname'=>$name[1],
            ], 'LIKE');
        return $this->with('languages')->get();
    }
}