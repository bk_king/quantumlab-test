<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 07.01.2018
 * Time: 22:22
 */

namespace Quantumlab\Database;


interface DatabaseInterface
{
    public function where($data, $operator=null);
    public function with($relation, $callable = null);
    public function attach($id);
    public function all();
    public function save();
    public function remove($data);
    public function get();
    public function first();

}