<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 07.01.2018
 * Time: 11:25
 */

namespace Quantumlab\Database;


class UsersLanguages extends Model
{
    protected $fillable=[
        'user_id',
        'language_id'
    ];
    protected $table = 'users_languages';
}