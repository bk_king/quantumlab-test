<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 29.12.2017
 * Time: 15:37
 */

namespace Quantumlab\Database;

use Quantumlab\Config\Config;
use Quantumlab\ModifyString\ModifyString;

class Model implements DatabaseInterface
{
    use Relations;
    private $database;
    protected $fillable = [];
    protected $table = '';
    private $result = [];

    public function __construct($items = [])
    {
        foreach ($items as $key => $item) {
            $this->$key = $item;
        }
        $this->database = $this->getDatabase();
    }

    public function with($relation, $callable = null)
    {
        $this->$relation();
        $this->relationName = $relation;
        if (!$this->isResultNotEmpty()) {
            $this->result = $this->all();
        }

        $pivotObj = new $this->pivot();
        $pivotObj->result = $pivotObj->all();
        //$this->$relation = [];

        foreach ($this->result as $key => $item) {
            $relations = [];
            foreach ($pivotObj->result as $pivotVal) {
                if ($item['id'] === $pivotVal[$this->local_key]) {

                    // $this->$relation[] = $this->getItem($pivotVal[$this->foreign_key]);
                    $relations[] = $this->getItem($pivotVal[$this->foreign_key]);
                }
            }
            $this->result[$key][$relation] = $relations;
        }
        return $this;
    }

    private function getItem($id)
    {
        $relationObj = new $this->relation();
        $relationObj->where(['id' => $id]);
        return $relationObj->isResultNotEmpty() ? $relationObj->result[0] : $relationObj->result;
    }

    public function all()
    {
        return $this->database[$this->table];
    }

    public function attach($id)
    {
        $data = [
            $this->local_key => $this->id,
            $this->foreign_key => $id
        ];

        $pivotObj = new $this->pivot($data);
        if(!$pivotObj->where($data)->first()){
            $pivotObj->save();
        }


    }

    public function save()
    {
        $properties = get_object_vars($this);
        $length = count($this->fillable);
        $counter = 0;
        foreach ($this->fillable as $item) {
            foreach ($properties as $key => $property) {
                if ($item === $key) {
                    $counter++;
                }
            }
        }
        if ($length === $counter) {
            $this->id = $this->setUniqueID();
            array_push($this->database[$this->table], $this);
            $this->saveDatabase($this->database);

        }
        return $this;
    }

    private function isResultNotEmpty()
    {
        return count($this->result) > 0;
    }

    public function where($data, $operator=null)
    {
        $counter = 0;
        $isResult = $this->isResultNotEmpty();
        $length = count($data);

        $elements = $isResult ? $this->result : $this->database[$this->table];

        foreach ($elements as $items) {
            foreach ($items as $key => $item) {
                foreach ($data as $itemKey => $itemVal) {
                    if(strtolower($operator) === 'like'){
                        if(ModifyString::toLower($itemKey) === ModifyString::toLower($key) && strpos($item, $itemVal) !== false){
                            $counter++;
                        }
                    }else{
                        if (ModifyString::toLower($itemKey) === ModifyString::toLower($key)
                            && ModifyString::toLower($itemVal) == ModifyString::toLower($item)) {
                            $counter++;
                        }
                    }
                }
            }
            if ($counter === $length) {
                $this->result[] = $items;
            }
            $counter = 0;
        }

        return $this;
    }

    public function get()
    {
        $className = get_class($this);
        $data = $this->isResultNotEmpty() ? $this->result : $this->database[$this->table];

        return array_map(function ($item) use ($className) {
            foreach ($item as $key => $val) {
                if (is_array($val)) {
                    $relationObj = new $this->relation();
                    $relationObj->result = $val;
                    $item[$this->relationName] = $relationObj->get();
                }
            }
            return new $className($item);
        }, $data);
    }

    public function first()
    {
        $className = get_class($this);
        if (count($this->result)) {
            return new $className($this->result[0]);
        }
        return null;

    }

    public function remove($data)
    {
        foreach ($this->database[$this->table] as $key => $item) {
            foreach ($data as $dataKey => $dataItem) {

                if ($item[$dataKey] == $dataItem) {
                    unset($this->database[$this->table][$key]);
                    $this->saveDatabase($this->database);
                    if($this->pivot){
                        $relationClass = new $this->pivot();
                        $relationClass->remove([$this->local_key=>$data['id']]);
                    }

                    echo "usunięto rekord  \n";
                }
            }
        }



    }

    private function setUniqueID()
    {
        $id = 0;
        $length = count($this->database[$this->table]) - 1;
        if (!empty($this->database[$this->table][$length])) {
            $id = $this->database[$this->table][$length]['id'];
        }
        return ++$id;
    }

    private function getDatabase()
    {
        return json_decode(file_get_contents(Config::DATABASE_PATH), true);
    }

    private function saveDatabase($data)
    {
        return file_put_contents(Config::DATABASE_PATH, json_encode($data));
    }


}