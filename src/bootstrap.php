<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Quantumlab\Controllers\Controller;
use Quantumlab\Controllers\ControllerException;
try{
    $controller = new Controller();
    $controller->init($argv);
}catch(ControllerException $e){
    echo $e->getMessage();
}

