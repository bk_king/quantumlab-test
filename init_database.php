<?php
require_once 'vendor/autoload.php';
use Quantumlab\Config\Config;
$database = [
    'users'=>[],
    'languages'=>[],
    'users_languages'=>[]
];
$dbToJson = json_encode($database);
file_put_contents(Config::ABSOLUTE_PATH.'resource/database.json', $dbToJson);